use ast::{Cbor, Key};
use byteorder::{BigEndian, WriteBytesExt};
use error::EncodeError;
use std::io;
use types::{
    Tag,
    BYTES,
    TEXT,
    ARRAY,
    OBJECT,
    TAGGED
};

type Result<T> = ::std::result::Result<T, EncodeError>;

pub struct Encoder<W> {
    writer: W
}

impl<W: io::Write> Encoder<W> {
    pub fn new(w: W) -> Encoder<W> {
        Encoder { writer: w }
    }

    #[inline]
    pub fn u8(&mut self, x: u8) -> Result<&mut Self> {
        {
            let w = &mut self.writer;
            match x {
                0...23 => w.write_u8(x)?,
                _      => w.write_u8(24).and(w.write_u8(x))?
            }
        }
        Ok(self)
    }

    #[inline]
    pub fn u16(&mut self, x: u16) -> Result<&mut Self> {
        {
            let w = &mut self.writer;
            match x {
                0...23    => w.write_u8(x as u8)?,
                24...0xFF => w.write_u8(24).and(w.write_u8(x as u8))?,
                _         => w.write_u8(25).and(w.write_u16::<BigEndian>(x))?
            }
        }
        Ok(self)
    }

    #[inline]
    pub fn u32(&mut self, x: u32) -> Result<&mut Self> {
        {
            let w = &mut self.writer;
            match x {
                0...23         => w.write_u8(x as u8)?,
                24...0xFF      => w.write_u8(24).and(w.write_u8(x as u8))?,
                0x100...0xFFFF => w.write_u8(25).and(w.write_u16::<BigEndian>(x as u16))?,
                _              => w.write_u8(26).and(w.write_u32::<BigEndian>(x))?
            }
        }
        Ok(self)
    }

    #[inline]
    pub fn u64(&mut self, x: u64) -> Result<&mut Self> {
        {
            let w = &mut self.writer;
            match x {
                0...23                    => w.write_u8(x as u8)?,
                24...0xFF                 => w.write_u8(24).and(w.write_u8(x as u8))?,
                0x0100...0xFFFF           => w.write_u8(25).and(w.write_u16::<BigEndian>(x as u16))?,
                0x0010_0000...0xFFFF_FFFF => w.write_u8(26).and(w.write_u32::<BigEndian>(x as u32))?,
                _                         => w.write_u8(27).and(w.write_u64::<BigEndian>(x))?
            }
        }
        Ok(self)
    }

    #[inline]
    pub fn i8(&mut self, x: i8) -> Result<&mut Self> {
        if x >= 0 {
            return self.u8(x as u8)
        }
        {
            let w = &mut self.writer;
            match (-1 - x) as u8 {
                n @ 0...23 => w.write_u8(0b001_00000 | n)?,
                n          => w.write_u8(0b001_00000 | 24).and(w.write_u8(n))?
            }
        }
        Ok(self)
    }

    #[inline]
    pub fn i16(&mut self, x: i16) -> Result<&mut Self> {
        if x >= 0 {
            return self.u16(x as u16)
        }
        {
            let w = &mut self.writer;
            match (-1 - x) as u16 {
                n @ 0...23    => w.write_u8(0b001_00000 | n as u8)?,
                n @ 24...0xFF => w.write_u8(0b001_00000 | 24).and(w.write_u8(n as u8))?,
                n             => w.write_u8(0b001_00000 | 25).and(w.write_u16::<BigEndian>(n))?
            }
        }
        Ok(self)
    }

    #[inline]
    pub fn i32(&mut self, x: i32) -> Result<&mut Self> {
        if x >= 0 {
            return self.u32(x as u32)
        }
        {
            let w = &mut self.writer;
            match (-1 - x) as u32 {
                n @ 0...23         => w.write_u8(0b001_00000 | n as u8)?,
                n @ 24...0xFF      => w.write_u8(0b001_00000 | 24).and(w.write_u8(n as u8))?,
                n @ 0x100...0xFFFF => w.write_u8(0b001_00000 | 25).and(w.write_u16::<BigEndian>(n as u16))?,
                n                  => w.write_u8(0b001_00000 | 26).and(w.write_u32::<BigEndian>(n))?
            }
        }
        Ok(self)
    }

    #[inline]
    pub fn i64(&mut self, x: i64) -> Result<&mut Self> {
        if x >= 0 {
            return self.u64(x as u64)
        }
        {
            let w = &mut self.writer;
            match (-1 - x) as u64 {
                n @ 0...23                    => w.write_u8(0b001_00000 | n as u8)?,
                n @ 24...0xFF                 => w.write_u8(0b001_00000 | 24).and(w.write_u8(n as u8))?,
                n @ 0x0100...0xFFFF           => w.write_u8(0b001_00000 | 25).and(w.write_u16::<BigEndian>(n as u16))?,
                n @ 0x0010_0000...0xFFFF_FFFF => w.write_u8(0b001_00000 | 26).and(w.write_u32::<BigEndian>(n as u32))?,
                n                             => w.write_u8(0b001_00000 | 27).and(w.write_u64::<BigEndian>(n))?
            }
        }
        Ok(self)
    }

    #[inline]
    pub fn f32(&mut self, x: f32) -> Result<&mut Self> {
        self.writer.write_u8(0b111_00000 | 26)
            .and(self.writer.write_f32::<BigEndian>(x))?;
        Ok(self)
    }

    #[inline]
    pub fn f64(&mut self, x: f64) -> Result<&mut Self> {
        self.writer.write_u8(0b111_00000 | 27)
            .and(self.writer.write_f64::<BigEndian>(x))?;
        Ok(self)

    }

    #[inline]
    pub fn bool(&mut self, x: bool) -> Result<&mut Self> {
        self.writer.write_u8(0b111_00000 | if x {21} else {20})?;
        Ok(self)
    }

    #[inline]
    pub fn char(&mut self, x: char) -> Result<&mut Self> {
        let mut buf = [0; 4];
        self.str(x.encode_utf8(&mut buf))
    }

    #[inline]
    pub fn null(&mut self) -> Result<&mut Self> {
        self.writer.write_u8(0b111_00000 | 22)?;
        Ok(self)
    }

    #[inline]
    pub fn undefined(&mut self) -> Result<&mut Self> {
        self.writer.write_u8(0b111_00000 | 23)?;
        Ok(self)
    }

    #[inline]
    pub fn tag(&mut self, x: Tag) -> Result<&mut Self> {
        self.type_len(TAGGED, x.to())
    }

    #[inline]
    pub fn simple(&mut self, x: u8) -> Result<&mut Self> {
        {
            let w = &mut self.writer;
            match x {
                0...19  => w.write_u8(0b111_00000 | x)?,
                20...31 => return Err(EncodeError::Simple(x)),
                _       => w.write_u8(0b111_00000 | 24).and(w.write_u8(x))?
            }
        }
        Ok(self)
    }

    #[inline]
    pub fn bytes(&mut self, x: &[u8]) -> Result<&mut Self> {
        self.type_len(BYTES, x.len() as u64)?.writer.write_all(x)?;
        Ok(self)
    }

    #[inline]
    pub fn bytes_begin(&mut self) -> Result<&mut Self> {
        self.writer.write_u8(0b010_11111)?;
        Ok(self)
    }

    #[inline]
    pub fn str(&mut self, x: &str) -> Result<&mut Self> {
        self.type_len(TEXT, x.len() as u64)?.writer.write_all(x.as_bytes())?;
        Ok(self)
    }

    #[inline]
    pub fn str_begin(&mut self) -> Result<&mut Self> {
        self.writer.write_u8(0b011_11111)?;
        Ok(self)
    }

    #[inline]
    pub fn array(&mut self, len: usize) -> Result<&mut Self> {
        self.type_len(ARRAY, len as u64)
    }

    #[inline]
    pub fn array_begin(&mut self) -> Result<&mut Self> {
        self.writer.write_u8(0b100_11111)?;
        Ok(self)
    }

    #[inline]
    pub fn end(&mut self) -> Result<&mut Self> {
        self.writer.write_u8(0b111_11111)?;
        Ok(self)
    }

    #[inline]
    pub fn object(&mut self, len: usize) -> Result<&mut Self> {
        self.type_len(OBJECT, len as u64)
    }

    #[inline]
    pub fn object_begin(&mut self) -> Result<&mut Self> {
        self.writer.write_u8(0b101_11111)?;
        Ok(self)
    }

    #[inline]
    pub fn vector<T, F>(&mut self, v: &[T], mut f: F) -> Result<&mut Self>
        where F: FnMut(&mut Self, &T) -> Result<()>
    {
        self.array(v.len())?;
        for x in v {
            f(self, x)?;
        }
        Ok(self)
    }

    #[inline]
    pub fn option<T, F>(&mut self, opt: &Option<T>, mut f: F) -> Result<&mut Self>
        where F: FnMut(&mut Self, &T) -> Result<()>
    {
        if let Some(ref v) = *opt {
            f(self, v)?;
            Ok(self)
        } else {
            self.null()
        }
    }

    pub fn cbor(&mut self, cbor: &Cbor) -> Result<&mut Self> {
        match *cbor {
            Cbor::Tagged(t, ref x) => self.tag(t)?.cbor(x),
            Cbor::Undefined        => self.undefined(),
            Cbor::Null             => self.null(),
            Cbor::Break            => Ok(self),
            Cbor::Simple(s)        => self.simple(s),
            Cbor::Bool(b)          => self.bool(b),
            Cbor::U8(n)            => self.u8(n),
            Cbor::U16(n)           => self.u16(n),
            Cbor::U32(n)           => self.u32(n),
            Cbor::U64(n)           => self.u64(n),
            Cbor::F16(n)           => self.f32(n),
            Cbor::F32(n)           => self.f32(n),
            Cbor::F64(n)           => self.f64(n),
            Cbor::I8(n)            => self.i8(n),
            Cbor::I16(n)           => self.i16(n),
            Cbor::I32(n)           => self.i32(n),
            Cbor::I64(n)           => self.i64(n),
            Cbor::Str(ref x)       => self.str(x),
            Cbor::Bytes(ref x)     => self.bytes(x),
            Cbor::Object(ref o)    => {
                self.object(o.len())?;
                for (k, v) in o {
                    self.key(k)?.cbor(v)?;
                }
                Ok(self)
            }
            Cbor::Array(ref vec) => {
                self.array(vec.len())?;
                for v in vec {
                    self.cbor(v)?;
                }
                Ok(self)
            }
            Cbor::StrChunks(ref xs) => {
                self.str_begin()?;
                for x in xs {
                    self.str(x)?;
                }
                self.end()
            }
            Cbor::BytesChunks(ref xs) => {
                self.bytes_begin()?;
                for x in xs {
                    self.bytes(x)?;
                }
                self.end()
            }
        }
    }

    pub fn key(&mut self, k: &Key) -> Result<&mut Self> {
        match *k {
            Key::Int(i)       => self.i64(i as i64),
            Key::Str(ref s)   => self.str(s),
            Key::Bytes(ref b) => self.bytes(b)
        }
    }

    #[inline]
    fn type_len(&mut self, t: u8, x: u64) -> Result<&mut Self> {
        {
            let w = &mut self.writer;
            match x {
                0...23                    => w.write_u8(t << 5 | x as u8)?,
                24...0xFF                 => w.write_u8(t << 5 | 24).and(w.write_u8(x as u8))?,
                0x0100...0xFFFF           => w.write_u8(t << 5 | 25).and(w.write_u16::<BigEndian>(x as u16))?,
                0x0010_0000...0xFFFF_FFFF => w.write_u8(t << 5 | 26).and(w.write_u32::<BigEndian>(x as u32))?,
                _                         => w.write_u8(t << 5 | 27).and(w.write_u64::<BigEndian>(x))?
            }
        }
        Ok(self)
    }
}

