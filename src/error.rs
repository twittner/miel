use std::{error, fmt, io, str};

#[derive(Debug)]
pub enum EncodeError {
    Io(io::Error),
    Simple(u8),
    Other(Box<dyn error::Error + Send + Sync>),
    #[doc(hidden)]
    __Nonexhaustive
}

impl fmt::Display for EncodeError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            EncodeError::Io(e) => write!(f, "i/o error: {}", e),
            EncodeError::Simple(n) => write!(f, "invalid simple value: {}", n),
            EncodeError::Other(e) => write!(f, "other error: {}", e),
            EncodeError::__Nonexhaustive => f.write_str("__Nonexhaustive")
        }
    }
}

impl error::Error for EncodeError {
    fn cause(&self) -> Option<&dyn error::Error> {
        match self {
            EncodeError::Io(e) => Some(e),
            EncodeError::Other(e) => Some(&**e),
            _ => None
        }
    }
}


impl From<io::Error> for EncodeError {
    fn from(e: io::Error) -> Self {
        EncodeError::Io(e)
    }
}


#[derive(Debug)]
pub enum DecodeError {
    InvalidChar,
    Utf8(str::Utf8Error),
    Overflow(u64),
    MissingKey(&'static str),
    InvalidKey,
    MissingBreak,
    EndOfInput,
    Type(u8, u8),
    Recursion,
    Other(Box<dyn error::Error + Send + Sync>),
    #[doc(hidden)]
    __Nonexhaustive
}

impl fmt::Display for DecodeError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            DecodeError::InvalidChar => f.write_str("invalid char"),
            DecodeError::Utf8(e) => write!(f, "invalid utf-8: {}", e),
            DecodeError::Overflow(n) => write!(f, "signed integer {} is greater than its max value", n),
            DecodeError::MissingKey(k) => write!(f, "object key '{}' is missing", k),
            DecodeError::InvalidKey => f.write_str("object key is invalid"),
            DecodeError::MissingBreak => f.write_str("break value not found"),
            DecodeError::EndOfInput => f.write_str("unexpected end of input"),
            DecodeError::Type(m, i) => write!(f, "type error (major: {}, info: {})", m, i),
            DecodeError::Recursion => f.write_str("maximum recursion leven reacher"),
            DecodeError::Other(e) => write!(f, "other error: {}", e),
            DecodeError::__Nonexhaustive => f.write_str("__Nonexhaustive")
        }
    }
}

impl error::Error for DecodeError {
    fn cause(&self) -> Option<&dyn error::Error> {
        match self {
            DecodeError::Utf8(e) => Some(e),
            DecodeError::Other(e) => Some(&**e),
            _ => None
        }
    }
}

impl From<str::Utf8Error> for DecodeError {
    fn from(e: str::Utf8Error) -> Self {
        DecodeError::Utf8(e)
    }
}

