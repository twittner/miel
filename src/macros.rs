#[macro_export]
macro_rules! encode_object {
    (
      let encoder = $enc: ident;
      $T: ident ($key_action: ident, $size: expr) {
          $($name: ident: $key: expr => $action: expr),+
      }
    ) => {{
        $enc.object($size)?;
        $($enc.$key_action($key)?; $action;)+
        Ok(())
    }}
}

#[macro_export]
macro_rules! decode_object {
    (
      let decoder = $dec: ident;
      $T: ident ($key_action: ident) {
          $($name: ident: $key: expr => $action: expr),+
      }
    ) => {{
        let size = $dec.object()?.ok_or($crate::DecodeError::Type(5, 31))?;
        $(let mut $name = None;)+
        for _ in 0 .. size {
            match $dec.$key_action()? {
                $($key => { $name = Some($action) })+
                _      => { $dec.skip()? }
            }
        }
        Ok($T {
            $($name:
                if let Some(e) = $name {
                    e
                } else {
                    let msg = stringify!($name [$key]);
                    return Err(::std::convert::From::from($crate::DecodeError::MissingKey(msg)))
                },
            )+
        })
    }}
}
