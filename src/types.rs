// (0,  n) = (UInt8, n),
// (0, 25) = (UInt16, 25),
// (0, 26) = (UInt32, 26),
// (0, 27) = (UInt64, 27),
// (1,  n) = (Int8, n),
// (1, 25) = (Int16, 25),
// (1, 26) = (Int32, 26),
// (1, 27) = (Int64, 27),
// (2, n)  = (Bytes, n),
// (3, n)  = (Text, n),
// (4, n)  = (Array, n),
// (5, n)  = (Object, n),
// (6, n)  = (Tagged, n),
// (7, 20) = (Bool, 20),
// (7, 21) = (Bool, 21),
// (7, 22) = (Null, 22),
// (7, 23) = (Undefined, 23),
// (7, 25) = (Float16, 25),
// (7, 26) = (Float32, 26),
// (7, 27) = (Float64, 27),
// (7, 31) = (Break, 31),
// (7, n)  = (Simple, n),

#[inline]
pub fn typeinfo(b: u8) -> (u8, u8) {
    ((b & 0b111_00000) >> 5, b & 0b000_11111)
}

pub const UNSIGNED: u8 = 0;
pub const SIGNED: u8 = 1;
pub const BYTES: u8 = 2;
pub const TEXT: u8 = 3;
pub const ARRAY: u8 = 4;
pub const OBJECT: u8 = 5;
pub const TAGGED: u8 = 6;
pub const SIMPLE: u8 = 7;

#[derive(Clone, Copy, PartialEq, PartialOrd, Eq, Ord, Debug, Hash)]
pub enum Tag {
    DateTime,
    Timestamp,
    Bignum,
    NegativeBignum,
    Decimal,
    Bigfloat,
    Unassigned(u64),
    ToBase64Url,
    ToBase64,
    ToBase16,
    Cbor,
    Uri,
    Base64Url,
    Base64,
    Regex,
    Mime,
    CborSelf
}

impl Tag {
    pub fn of(x: u64) -> Tag {
        match x {
            0     => Tag::DateTime,
            1     => Tag::Timestamp,
            2     => Tag::Bignum,
            3     => Tag::NegativeBignum,
            4     => Tag::Decimal,
            5     => Tag::Bigfloat,
            21    => Tag::ToBase64Url,
            22    => Tag::ToBase64,
            23    => Tag::ToBase16,
            24    => Tag::Cbor,
            32    => Tag::Uri,
            33    => Tag::Base64Url,
            34    => Tag::Base64,
            35    => Tag::Regex,
            36    => Tag::Mime,
            55799 => Tag::CborSelf,
            _     => Tag::Unassigned(x)
        }
    }

    pub fn to(&self) -> u64 {
        match *self {
            Tag::DateTime       => 0,
            Tag::Timestamp      => 1,
            Tag::Bignum         => 2,
            Tag::NegativeBignum => 3,
            Tag::Decimal        => 4,
            Tag::Bigfloat       => 5,
            Tag::ToBase64Url    => 21,
            Tag::ToBase64       => 22,
            Tag::ToBase16       => 23,
            Tag::Cbor           => 24,
            Tag::Uri            => 32,
            Tag::Base64Url      => 33,
            Tag::Base64         => 34,
            Tag::Regex          => 35,
            Tag::Mime           => 36,
            Tag::CborSelf       => 55799,
            Tag::Unassigned(x)  => x
        }
    }
}

