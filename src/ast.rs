use std::borrow::Cow;
use std::collections::BTreeMap;
use std::isize;
use std::ops::Deref;
use types::Tag;

#[derive(Clone, Debug)]
pub enum Cbor<'buf> {
    Array(Vec<Cbor<'buf>>),
    Bool(bool),
    Break,
    Bytes(Cow<'buf, [u8]>),
    BytesChunks(Vec<Cow<'buf, [u8]>>),
    F16(f32),
    F32(f32),
    F64(f64),
    I8(i8),
    I16(i16),
    I32(i32),
    I64(i64),
    Object(BTreeMap<Key<'buf>, Cbor<'buf>>),
    Null,
    Simple(u8),
    Tagged(Tag, Box<Cbor<'buf>>),
    Str(Cow<'buf, str>),
    StrChunks(Vec<Cow<'buf, str>>),
    U8(u8),
    U16(u16),
    U32(u32),
    U64(u64),
    Undefined
}

#[derive(Clone, Debug, PartialEq, PartialOrd, Eq, Ord, Hash)]
pub enum Key<'buf> {
    Int(isize),
    Bytes(Cow<'buf, [u8]>),
    Str(Cow<'buf, str>)
}

impl<'buf> Key<'buf> {
    pub fn from_value(v: Cbor<'buf>) -> Option<Key<'buf>> {
        match v {
            Cbor::Bytes(x) => Some(Key::Bytes(x)),
            Cbor::Str(x)   => Some(Key::Str(x)),
            Cbor::I8(x)    => Some(Key::Int(x as isize)),
            Cbor::I16(x)   => Some(Key::Int(x as isize)),
            Cbor::I32(x)   => Some(Key::Int(x as isize)),
            Cbor::I64(x)   =>
                if x > isize::MAX as i64 || x < isize::MIN as i64 {
                    None
                } else {
                    Some(Key::Int(x as isize))
                },
            Cbor::U8(x)    => Some(Key::Int(x as isize)),
            Cbor::U16(x)   => Some(Key::Int(x as isize)),
            Cbor::U32(x)   => Some(Key::Int(x as isize)),
            Cbor::U64(x)   =>
                if x > isize::MAX as u64 {
                    None
                } else {
                    Some(Key::Int(x as isize))
                },
            _ => None
        }
    }
}

impl<'a> From<&'a str> for Key<'a> {
    fn from(s: &'a str) -> Key<'a> {
        Key::Str(Cow::Borrowed(s))
    }
}

impl<'a> From<isize> for Key<'a> {
    fn from(i: isize) -> Key<'a> {
        Key::Int(i)
    }
}

pub struct Ref<'a, 'buf: 'a> {
    value: Option<&'a Cbor<'buf>>
}

impl<'a, 'buf> Ref<'a, 'buf> {
    pub fn new(v: &'a Cbor<'buf>) -> Ref<'a, 'buf> {
        Ref { value: Some(v) }
    }

    fn of(v: Option<&'a Cbor<'buf>>) -> Ref<'a, 'buf> {
        Ref { value: v }
    }

    pub fn at(&self, i: usize) -> Ref<'a, 'buf> {
        if let Some(&Cbor::Array(ref a)) = self.value {
            Ref::of(a.get(i))
        } else {
            Ref::of(None)
        }
    }

    pub fn get<K: Into<Key<'a>>>(&self, k: K) -> Ref<'a, 'buf> {
        if let Some(&Cbor::Object(ref m)) = self.value {
            Ref::of(m.get(&k.into()))
        } else {
            Ref::of(None)
        }
    }

    pub fn option(&self) -> Option<Ref<'a, 'buf>> {
        match self.value {
            Some(&Cbor::Null) => None,
            Some(ref v)       => Some(Ref::new(v)),
            _                 => None
        }
    }

    pub fn bool(&self) -> Option<bool> {
        if let Some(&Cbor::Bool(x)) = self.value {
            Some(x)
        } else {
            None
        }
    }

    pub fn str(&self) -> Option<&Cow<'buf, str>> {
        if let Some(&Cbor::Str(ref x)) = self.value {
            Some(x)
        } else {
            None
        }
    }

    pub fn str_chunks(&self) -> Option<&[Cow<'buf, str>]> {
        if let Some(&Cbor::StrChunks(ref x)) = self.value {
             Some(&x)
        } else {
            None
        }
    }

    pub fn bytes(&self) -> Option<&Cow<'buf, [u8]>> {
        if let Some(&Cbor::Bytes(ref x)) = self.value {
             Some(x)
        } else {
            None
        }
    }

    pub fn bytes_chunks(&self) -> Option<&[Cow<'buf, [u8]>]> {
        if let Some(&Cbor::BytesChunks(ref x)) = self.value {
             Some(&x)
        } else {
            None
        }
    }

    pub fn array(&'a self) -> Option<&'a [Cbor<'buf>]> {
        if let Some(&Cbor::Array(ref v)) = self.value {
            Some(v)
        } else {
            None
        }
    }

    pub fn float(&self) -> Option<f64> {
        match self.value {
            Some(&Cbor::F16(x)) => Some(f64::from(x)),
            Some(&Cbor::F32(x)) => Some(f64::from(x)),
            Some(&Cbor::F64(x)) => Some(x),
            _                   => None
        }
    }

    pub fn signed(&self) -> Option<i64> {
        match self.value {
            Some(&Cbor::I8(x))  => Some(i64::from(x)),
            Some(&Cbor::I16(x)) => Some(i64::from(x)),
            Some(&Cbor::I32(x)) => Some(i64::from(x)),
            Some(&Cbor::I64(x)) => Some(x),
            _                   => None
        }
    }

    pub fn unsigned(&self) -> Option<u64> {
        match self.value {
            Some(&Cbor::U8(x))  => Some(u64::from(x)),
            Some(&Cbor::U16(x)) => Some(u64::from(x)),
            Some(&Cbor::U32(x)) => Some(u64::from(x)),
            Some(&Cbor::U64(x)) => Some(x),
            _                   => None
        }
    }

    pub fn is_undefined(&self) -> bool {
        if let Some(&Cbor::Undefined) = self.value {
            true
        } else {
            false
        }
    }

    pub fn is_null(&self) -> bool {
        if let Some(&Cbor::Null) = self.value {
            true
        } else {
            false
        }
    }

    pub fn is_break(&self) -> bool {
        if let Some(&Cbor::Break) = self.value {
            true
        } else {
            false
        }
    }

    pub fn simple(&self) -> Option<u8> {
        if let Some(&Cbor::Simple(x)) = self.value {
            Some(x)
        } else {
            None
        }
    }

    pub fn tag(&'a self) -> Option<(&'a Tag, &'a Cbor<'buf>)> {
        if let Some(&Cbor::Tagged(ref t, ref c)) = self.value {
            Some((t, c))
        } else {
            None
        }
    }
}

impl<'a, 'buf> Deref for Ref<'a, 'buf> {
    type Target = Option<&'a Cbor<'buf>>;

    fn deref(&self) -> &Self::Target {
        &self.value
    }
}

