use error::{EncodeError, DecodeError};
use sd::{de, ser};
use std::{error, fmt};

pub type Result<T> = ::std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    Encode(EncodeError),
    Decode(DecodeError)
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::Encode(e) => write!(f, "serialization error: {}", e),
            Error::Decode(e) => write!(f, "deserialization error: {}", e)
        }
    }
}

impl error::Error for Error {
    fn cause(&self) -> Option<&dyn error::Error> {
        match self {
            Error::Encode(e) => Some(e),
            Error::Decode(e) => Some(e)
        }
    }
}

impl From<EncodeError> for Error {
    fn from(e: EncodeError) -> Error {
        Error::Encode(e)
    }
}

impl From<DecodeError> for Error {
    fn from(e: DecodeError) -> Error {
        Error::Decode(e)
    }
}

impl ser::Error for Error {
    fn custom<T: fmt::Display>(msg: T) -> Self {
        Error::Encode(EncodeError::Other(msg.to_string().into()))
    }
}

impl de::Error for Error {
    fn custom<T: fmt::Display>(msg: T) -> Self {
        Error::Decode(DecodeError::Other(msg.to_string().into()))
    }
}

