use error::DecodeError;
use slice::Decoder;
use serde::error::{Error, Result};
use sd::de::{self, DeserializeSeed, Visitor};
use types::{
    typeinfo,
    UNSIGNED,
    SIGNED,
    BYTES,
    TEXT,
    ARRAY,
    OBJECT,
    SIMPLE
};

pub struct Deserializer<'de> {
    decoder: Decoder<'de>
}

impl<'de> Deserializer<'de> {
    pub fn from_bytes(b: &'de [u8]) -> Self {
        Deserializer { decoder: Decoder::new(b) }
    }
}

impl<'a, 'de> de::Deserializer<'de> for &'a mut Deserializer<'de> {
    type Error = Error;

    fn deserialize_any<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        match typeinfo(self.decoder.peek_byte()?) {
            (UNSIGNED, 0...24) => self.deserialize_u8(v),
            (UNSIGNED,     25) => self.deserialize_u16(v),
            (UNSIGNED,     26) => self.deserialize_u32(v),
            (UNSIGNED,     27) => self.deserialize_u64(v),
            (SIGNED,   0...24) => self.deserialize_i8(v),
            (SIGNED,       25) => self.deserialize_i16(v),
            (SIGNED,       26) => self.deserialize_i32(v),
            (SIGNED,       27) => self.deserialize_i64(v),
            (SIMPLE,       25) => v.visit_f32(self.decoder.f16()?),
            (SIMPLE,       26) => self.deserialize_f32(v),
            (SIMPLE,       27) => self.deserialize_f64(v),
            (SIMPLE,  20...21) => self.deserialize_bool(v),
            (BYTES,         _) => self.deserialize_bytes(v),
            (TEXT,          _) => self.deserialize_str(v),
            (ARRAY,         _) => self.deserialize_seq(v),
            (OBJECT,        _) => self.deserialize_map(v),
            (major_type, info) => Err(Error::Decode(DecodeError::Type(major_type, info)))
        }
    }

    fn deserialize_bool<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        v.visit_bool(self.decoder.bool()?)
    }

    fn deserialize_i8<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        v.visit_i8(self.decoder.i8()?)
    }

    fn deserialize_i16<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        v.visit_i16(self.decoder.i16()?)
    }

    fn deserialize_i32<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        v.visit_i32(self.decoder.i32()?)
    }

    fn deserialize_i64<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        v.visit_i64(self.decoder.i64()?)
    }

    fn deserialize_u8<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        v.visit_u8(self.decoder.u8()?)
    }

    fn deserialize_u16<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        v.visit_u16(self.decoder.u16()?)
    }

    fn deserialize_u32<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        v.visit_u32(self.decoder.u32()?)
    }

    fn deserialize_u64<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        v.visit_u64(self.decoder.u64()?)
    }

    fn deserialize_f32<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        v.visit_f32(self.decoder.f32()?)
    }

    fn deserialize_f64<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        v.visit_f64(self.decoder.f64()?)
    }

    fn deserialize_char<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        v.visit_char(self.decoder.char()?)
    }

    fn deserialize_str<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        v.visit_borrowed_str(self.decoder.str()?)
    }

    fn deserialize_string<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        self.deserialize_str(v)
    }

    fn deserialize_bytes<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        v.visit_borrowed_bytes(self.decoder.bytes()?)
    }

    fn deserialize_byte_buf<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        self.deserialize_bytes(v)
    }

    fn deserialize_option<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        if self.decoder.is_null()? {
            self.decoder.skip_bytes(1)?;
            v.visit_none()
        } else {
            v.visit_some(self)
        }
    }

    fn deserialize_unit<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        if self.decoder.is_null()? {
            self.decoder.skip_bytes(1)?;
            v.visit_unit()
        } else {
            Err(de::Error::custom("expected null"))
        }
    }

    fn deserialize_unit_struct<V>(self, _name: &'static str, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        self.deserialize_unit(v)
    }

    fn deserialize_newtype_struct<V>(self, _name: &'static str, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        v.visit_newtype_struct(self)
    }

    fn deserialize_seq<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        let n = self.decoder.array()?;
        v.visit_seq(Sequence::new(self, n))
    }

    fn deserialize_tuple<V>(self, _len: usize, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        if let Some(len) = self.decoder.array()? {
            v.visit_seq(Sequence::new(self, Some(len)))
        } else {
            Err(de::Error::custom("tuple encoded as indefinite array"))
        }
    }

    fn deserialize_tuple_struct<V>(self, _name: &'static str, len: usize, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        self.deserialize_tuple(len, v)
    }

    fn deserialize_map<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        let n = self.decoder.object()?;
        v.visit_map(Sequence::new(self, n))
    }

    fn deserialize_struct<V>(self, _name: &'static str, _fields: &'static [&'static str], v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        self.deserialize_map(v)
    }

    fn deserialize_enum<V>(self, _name: &'static str, _variants: &'static [&'static str], v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        v.visit_enum(Enum::new(self))
    }

    fn deserialize_identifier<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        self.deserialize_str(v)
    }

    fn deserialize_ignored_any<V>(self, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        self.deserialize_any(v)
    }

    fn is_human_readable(&self) -> bool {
        false
    }
}

struct Enum<'a, 'de: 'a> {
    deserializer: &'a mut Deserializer<'de>
}

impl<'a, 'de> Enum<'a, 'de> {
    fn new(d: &'a mut Deserializer<'de>) -> Enum<'a, 'de> {
        Enum { deserializer: d }
    }
}

impl<'a, 'de> de::EnumAccess<'de> for Enum<'a, 'de> {
    type Error = Error;
    type Variant = Self;

    fn variant_seed<V>(self, seed: V) -> Result<(V::Value, Self::Variant)>
        where V: DeserializeSeed<'de>
    {
        seed.deserialize(&mut *self.deserializer).map(|v| (v, self))
    }
}

impl<'a, 'de> de::VariantAccess<'de> for Enum<'a, 'de> {
    type Error = Error;

    fn unit_variant(self) -> Result<()> {
        Ok(())
    }

    fn newtype_variant_seed<T>(self, seed: T) -> Result<T::Value>
        where T: DeserializeSeed<'de>
    {
        seed.deserialize(self.deserializer)
    }

    fn tuple_variant<V>(self, len: usize, v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        de::Deserializer::deserialize_tuple(self.deserializer, len, v)
    }

    fn struct_variant<V>(self, _fields: &'static [&'static str], v: V) -> Result<V::Value>
        where V: Visitor<'de>
    {
        de::Deserializer::deserialize_map(self.deserializer, v)
    }
}

struct Sequence<'a, 'de: 'a> {
    deserializer: &'a mut Deserializer<'de>,
    length: Option<usize>
}

impl<'a, 'de> Sequence<'a, 'de> {
    fn new(d: &'a mut Deserializer<'de>, n: Option<usize>) -> Sequence<'a, 'de> {
        Sequence { deserializer: d, length: n }
    }
}

impl<'a, 'de> de::SeqAccess<'de> for Sequence<'a, 'de> {
    type Error = Error;

    fn next_element_seed<T>(&mut self, seed: T) -> Result<Option<T::Value>>
        where T: DeserializeSeed<'de>
    {
        match self.length {
            Some(0) => Ok(None),
            Some(n) => {
                let x = seed.deserialize(&mut *self.deserializer)?;
                self.length = Some(n - 1);
                Ok(Some(x))
            }
            None =>
                if self.deserializer.decoder.is_break()? {
                    Ok(None)
                } else {
                    seed.deserialize(&mut *self.deserializer).map(Some)
                }
        }
    }
}

impl<'a, 'de> de::MapAccess<'de> for Sequence<'a, 'de> {
    type Error = Error;

    fn next_key_seed<K>(&mut self, seed: K) -> Result<Option<K::Value>>
        where K: DeserializeSeed<'de>
    {
        match self.length {
            Some(0) => Ok(None),
            Some(_) => seed.deserialize(&mut *self.deserializer).map(Some),
            None    =>
                if self.deserializer.decoder.is_break()? {
                    Ok(None)
                } else {
                    seed.deserialize(&mut *self.deserializer).map(Some)
                }
        }
    }

    fn next_value_seed<V>(&mut self, seed: V) -> Result<V::Value>
        where V: DeserializeSeed<'de>
    {
        if let Some(n) = self.length {
            let x = seed.deserialize(&mut *self.deserializer)?;
            self.length = Some(n - 1);
            Ok(x)
        } else {
            seed.deserialize(&mut *self.deserializer)
        }
    }
}

