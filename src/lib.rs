extern crate byteorder;

#[cfg(feature="serde")]
extern crate serde as sd;

#[macro_use]
pub mod macros;

pub mod ast;
pub mod error;
pub mod types;
pub mod slice;
pub mod write;

#[cfg(feature="serde")]
pub mod serde;

pub use ast::{Cbor, Key, Ref};
pub use error::{DecodeError, EncodeError};
pub use types::Tag;
