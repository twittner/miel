use ast::{Cbor, Key};
use byteorder::{ByteOrder, BigEndian};
use error::DecodeError;
use std::borrow::Cow;
use std::collections::BTreeMap;
use std::{i8, i16, i32, i64, f32, f64, u64};
use std::str;
use types::{
    Tag,
    typeinfo,
    UNSIGNED,
    SIGNED,
    BYTES,
    TEXT,
    ARRAY,
    OBJECT,
    TAGGED,
    SIMPLE
};

macro_rules! match_type {
    ($read: expr; $($t: pat => $action: expr),+) => {{
        let b = $read;
        match ((b & 0b111_00000) >> 5, b & 0b000_11111) {
            $($t   => { $action })+
            (t, i) => Err(DecodeError::Type(t, i))
        }
    }};
}

macro_rules! checked_cast {
    (from: $e: expr, to: $to: ty, max: $max: expr) => {{
         $e.and_then(|n| {
             if n > $max {
                 Err(DecodeError::Overflow(u64::from(n)))
             } else {
                 Ok(n as $to)
             }
         })
    }}
}

macro_rules! as_signed {
    (from: $e: expr, to: $to: ty, max: $max: expr) => {{
         $e.and_then(|n| {
             if n > $max {
                 Err(DecodeError::Overflow(u64::from(n)))
             } else {
                 Ok(-1 - n as $to)
             }
         })
    }}
}

type Result<T> = ::std::result::Result<T, DecodeError>;

pub struct Decoder<'buf> {
    buf: &'buf [u8],
    len: usize,
    idx: usize
}

impl<'buf> Decoder<'buf> {
    pub fn new(b: &'buf[u8]) -> Decoder<'buf> {
        Decoder { buf: b, len: b.len(), idx: 0 }
    }

    #[inline]
    pub fn peek_byte(&self) -> Result<u8> {
        if self.idx < self.len {
            Ok(self.buf[self.idx])
        } else {
            Err(DecodeError::EndOfInput)
        }
    }

    #[inline]
    pub fn skip_bytes(&mut self, n: usize) -> Result<()> {
        self.read(n)?;
        Ok(())
    }

    #[inline]
    pub fn is_null(&self) -> Result<bool> {
        self.peek_byte().map(|b| typeinfo(b) == (SIMPLE, 22))
    }

    #[inline]
    pub fn is_break(&self) -> Result<bool> {
        self.peek_byte().map(|b| typeinfo(b) == (SIMPLE, 31))
    }

    #[inline]
    pub fn is_undefined(&self) -> Result<bool> {
        self.peek_byte().map(|b| typeinfo(b) == (SIMPLE, 23))
    }

    #[inline]
    pub fn bool(&mut self) -> Result<bool> {
        match_type! { self.next()?;
            (SIMPLE, 20) => Ok(false),
            (SIMPLE, 21) => Ok(true)
        }
    }

    #[inline]
    pub fn u8(&mut self) -> Result<u8> {
        match_type! { self.next()?;
            (UNSIGNED, n @ 0...23) => Ok(n),
            (UNSIGNED, 24) => self.next()
        }
    }

    #[inline]
    pub fn u16(&mut self) -> Result<u16> {
        match_type! { self.next()?;
            (UNSIGNED, n @ 0...23) => Ok(u16::from(n)),
            (UNSIGNED, 24) => self.next().map(u16::from),
            (UNSIGNED, 25) => self.read(2).map(BigEndian::read_u16)
        }
    }

    #[inline]
    pub fn u32(&mut self) -> Result<u32> {
        match_type! { self.next()?;
            (UNSIGNED, n @ 0...23) => Ok(u32::from(n)),
            (UNSIGNED, 24) => self.next().map(u32::from),
            (UNSIGNED, 25) => self.read(2).map(BigEndian::read_u16).map(u32::from),
            (UNSIGNED, 26) => self.read(4).map(BigEndian::read_u32)
        }
    }

    #[inline]
    pub fn u64(&mut self) -> Result<u64> {
        match_type! { self.next()?;
            (UNSIGNED, n @ 0...23) => Ok(u64::from(n)),
            (UNSIGNED, 24) => self.next().map(u64::from),
            (UNSIGNED, 25) => self.read(2).map(BigEndian::read_u16).map(u64::from),
            (UNSIGNED, 26) => self.read(4).map(BigEndian::read_u32).map(u64::from),
            (UNSIGNED, 27) => self.read(8).map(BigEndian::read_u64)
        }
    }

    #[inline]
    pub fn i8(&mut self) -> Result<i8> {
        match_type! { self.next()?;
            (SIGNED,   n @ 0...23) => Ok(-1 - n as i8),
            (SIGNED,           24) => as_signed!(from: self.next(), to: i8, max: i8::MAX as u8),
            (UNSIGNED, n @ 0...23) => Ok(n as i8),
            (UNSIGNED,         24) => checked_cast!(from: self.next(), to: i8, max: i8::MAX as u8)
        }
    }

    #[inline]
    pub fn i16(&mut self) -> Result<i16> {
        match_type! { self.next()?;
            (SIGNED,   n @ 0...23) => Ok(-1 - i16::from(n)),
            (SIGNED,           24) => self.next().map(|n| -1 - i16::from(n)),
            (SIGNED,           25) => as_signed!(from: self.read(2).map(BigEndian::read_u16), to: i16, max: i16::MAX as u16),
            (UNSIGNED, n @ 0...23) => Ok(i16::from(n)),
            (UNSIGNED,         24) => self.next().map(i16::from),
            (UNSIGNED,         25) => checked_cast!(from: self.read(2).map(BigEndian::read_u16), to: i16, max: i16::MAX as u16)
        }
    }

    #[inline]
    pub fn i32(&mut self) -> Result<i32> {
        match_type! { self.next()?;
            (SIGNED,   n @ 0...23) => Ok(-1 - i32::from(n)),
            (SIGNED,           24) => self.next().map(|n| -1 - i32::from(n)),
            (SIGNED,           25) => self.read(2).map(BigEndian::read_u16).map(|n| -1 - i32::from(n)),
            (SIGNED,           26) => as_signed!(from: self.read(4).map(BigEndian::read_u32), to: i32, max: i32::MAX as u32),
            (UNSIGNED, n @ 0...23) => Ok(i32::from(n)),
            (UNSIGNED,         24) => self.next().map(i32::from),
            (UNSIGNED,         25) => self.read(2).map(BigEndian::read_u16).map(i32::from),
            (UNSIGNED,         26) => checked_cast!(from: self.read(4).map(BigEndian::read_u32), to: i32, max: i32::MAX as u32)
        }
    }

    #[inline]
    pub fn i64(&mut self) -> Result<i64> {
        match_type! { self.next()?;
            (SIGNED,   n @ 0...23) => Ok(-1 - i64::from(n)),
            (SIGNED,           24) => self.next().map(|n| -1 - i64::from(n)),
            (SIGNED,           25) => self.read(2).map(BigEndian::read_u16).map(|n| -1 - i64::from(n)),
            (SIGNED,           26) => self.read(4).map(BigEndian::read_u32).map(|n| -1 - i64::from(n)),
            (SIGNED,           27) => as_signed!(from: self.read(8).map(BigEndian::read_u64), to: i64, max: i64::MAX as u64),
            (UNSIGNED, n @ 0...23) => Ok(i64::from(n)),
            (UNSIGNED,         24) => self.next().map(i64::from),
            (UNSIGNED,         25) => self.read(2).map(BigEndian::read_u16).map(i64::from),
            (UNSIGNED,         26) => self.read(4).map(BigEndian::read_u32).map(i64::from),
            (UNSIGNED,         27) => checked_cast!(from: self.read(8).map(BigEndian::read_u64), to: i64, max: i64::MAX as u64)
        }
    }

    #[inline]
    pub fn f16(&mut self) -> Result<f32> {
        match_type! { self.next()?;
            (SIMPLE, 25) => {
                // Copied from RFC 7049 Appendix D and replaced ldexpf usage:
                let half  = self.read(2).map(BigEndian::read_u16)?;
                let exp   = half >> 10 & 0x1F;
                let mant  = half & 0x3FF;
                let value = match exp {
                    0  => f32::from(mant) * (2f32).powi(-24),
                    31 => if mant == 0 { f32::INFINITY } else { f32::NAN },
                    _  => (f32::from(mant) + 1024.0) * (2f32).powi(i32::from(exp) - 25)
                };
                Ok(if half & 0x8000 == 0 { value } else { - value })
            }
        }
    }

    #[inline]
    pub fn f32(&mut self) -> Result<f32> {
        match_type! { self.next()?;
            (SIMPLE, 26) => self.read(4).map(BigEndian::read_f32)
        }
    }

    #[inline]
    pub fn f64(&mut self) -> Result<f64> {
        match_type! { self.next()?;
            (SIMPLE, 27) => self.read(8).map(BigEndian::read_f64)
        }
    }

    #[inline]
    pub fn simple(&mut self) -> Result<u8> {
        match_type! { self.next()?;
            (SIMPLE, n @ 0...19) => Ok(n),
            (SIMPLE,         24) => self.next()
        }
    }

    #[inline]
    pub fn char(&mut self) -> Result<char> {
        match_type! { self.next()?;
            (TEXT, i) => {
                if i > 4 {
                    Err(DecodeError::InvalidChar)
                } else {
                    let txt = str::from_utf8(self.read(i as usize)?)?;
                    Ok(txt.chars().next().unwrap())
                }
            }
        }
    }

    #[inline]
    pub fn bytes(&mut self) -> Result<&'buf [u8]> {
        let (t, i) = self.next().map(typeinfo)?;
        if t != BYTES || i == 31 {
            return Err(DecodeError::Type(t, i))
        }
        let n = self.unsigned(BYTES, i)?;
        self.read(n as usize)
    }

    #[inline]
    pub fn bytes_iter<'a>(&'a mut self) -> Result<BytesIter<'a, 'buf>> {
        match_type! { self.next()?;
            (BYTES, 31) => Ok(BytesIter::new(self))
        }
    }

    #[inline]
    pub fn str(&mut self) -> Result<&'buf str> {
        let (t, i) = self.next().map(typeinfo)?;
        if t != TEXT || i == 31 {
            return Err(DecodeError::Type(t, i))
        }
        let n = self.unsigned(TEXT, i)?;
        let d = self.read(n as usize)?;
        str::from_utf8(d).map_err(From::from)
    }

    #[inline]
    pub fn str_iter<'a>(&'a mut self) -> Result<StrIter<'a, 'buf>> {
        match_type! { self.next()?;
            (TEXT, 31) => Ok(StrIter::new(self))
        }
    }

    #[inline]
    pub fn tag(&mut self) -> Result<Tag> {
        match_type! { self.next()?;
            (TAGGED, i) => self.unsigned(TAGGED, i).map(Tag::of)
        }
    }

    #[inline]
    pub fn array(&mut self) -> Result<Option<usize>> {
        match_type! { self.next()?;
            (ARRAY, 31) => Ok(None),
            (ARRAY,  a) => Ok(Some(self.unsigned(ARRAY, a)? as usize))
        }
    }

    #[inline]
    pub fn vector<T, F>(&mut self, mut f: F) -> Result<Vec<T>>
        where F: FnMut(&mut Self) -> Result<T>
    {
        let mut v = Vec::new();
        if let Some(n) = self.array()? {
            for _ in 0 .. n {
                v.push(f(self)?);
            }
        } else {
            while !self.is_break()? {
                v.push(f(self)?);
            }
        }
        Ok(v)
    }

    #[inline]
    pub fn object(&mut self) -> Result<Option<usize>> {
        match_type! { self.next()?;
            (OBJECT, 31) => Ok(None),
            (OBJECT,  a) => Ok(Some(self.unsigned(OBJECT, a)? as usize))
        }
    }

    #[inline]
    pub fn option<T, F>(&mut self, mut f: F) -> Result<Option<T>>
        where F: FnMut(&mut Self) -> Result<T>
    {
        if self.is_null()? {
            self.skip_bytes(1)?;
            return Ok(None)
        }
        f(self).map(Some)
    }

    pub fn cbor(&mut self, max_recur: usize) -> Result<Cbor<'buf>> {
        if max_recur == 0 {
            return Err(DecodeError::Recursion)
        }
        match typeinfo(self.peek_byte()?) {
            (UNSIGNED, 0...23) => self.u8().map(Cbor::U8),
            (UNSIGNED,     24) => self.u8().map(Cbor::U8),
            (UNSIGNED,     25) => self.u16().map(Cbor::U16),
            (UNSIGNED,     26) => self.u32().map(Cbor::U32),
            (UNSIGNED,     27) => self.u64().map(Cbor::U64),
            (SIGNED,   0...23) => self.i8().map(Cbor::I8),
            (SIGNED,       24) => self.i16().map(Cbor::I16),
            (SIGNED,       25) => self.i16().map(Cbor::I16),
            (SIGNED,       26) => self.i32().map(Cbor::I32),
            (SIGNED,       27) => self.i64().map(Cbor::I64),
            (SIMPLE,       20) => self.skip_bytes(1).and(Ok(Cbor::Bool(false))),
            (SIMPLE,       21) => self.skip_bytes(1).and(Ok(Cbor::Bool(true))),
            (SIMPLE,       22) => self.skip_bytes(1).and(Ok(Cbor::Null)),
            (SIMPLE,       23) => self.skip_bytes(1).and(Ok(Cbor::Undefined)),
            (SIMPLE,       25) => self.f16().map(Cbor::F16),
            (SIMPLE,       26) => self.f32().map(Cbor::F32),
            (SIMPLE,       27) => self.f64().map(Cbor::F64),
            (SIMPLE,       31) => self.skip_bytes(1).and(Ok(Cbor::Break)),
            (SIMPLE,        _) => self.simple().map(Cbor::Simple),
            (BYTES,        31) => {
                let mut it = self.bytes_iter()?;
                let mut chunks = Vec::new();
                for c in &mut it {
                    chunks.push(Cow::Borrowed(c?));
                }
                if let Some(e) = it.error.take() {
                    return Err(e)
                }
                Ok(Cbor::BytesChunks(chunks))
            }
            (BYTES,         _) => self.bytes().map(|b| Cbor::Bytes(Cow::Borrowed(b))),
            (TEXT,         31) => {
                let mut it = self.str_iter()?;
                let mut chunks = Vec::new();
                for c in &mut it {
                    chunks.push(Cow::Borrowed(c?));
                }
                if let Some(e) = it.error.take() {
                    return Err(e)
                }
                Ok(Cbor::StrChunks(chunks))
            }
            (TEXT,          _) => self.str().map(|s| Cbor::Str(Cow::Borrowed(s))),
            (TAGGED,        _) => {
                let t = self.tag()?;
                self.cbor(max_recur - 1).map(|v| Cbor::Tagged(t, Box::new(v)))
            }
            (ARRAY,        31) => {
                self.skip_bytes(1)?;
                let mut v = Vec::new();
                loop {
                    match self.cbor(max_recur - 1)? {
                        Cbor::Break => break,
                        value       => v.push(value)
                    }
                }
                Ok(Cbor::Array(v))
            }
            (OBJECT,       31) => {
                self.skip_bytes(1)?;
                let mut m = BTreeMap::new();
                loop {
                    match self.cbor(max_recur - 1)? {
                        Cbor::Break => break,
                        value        => {
                            if let Some(k) = Key::from_value(value) {
                                m.insert(k, self.cbor(max_recur - 1)?);
                            } else {
                                return Err(DecodeError::InvalidKey)
                            }
                        }
                    }
                }
                Ok(Cbor::Object(m))
            }
            (ARRAY,         n) => {
                self.skip_bytes(1)?;
                let mut v = Vec::new();
                for _ in 0 .. self.unsigned(ARRAY, n)? {
                    v.push(self.cbor(max_recur - 1)?)
                }
                Ok(Cbor::Array(v))
            }
            (OBJECT,        n) => {
                self.skip_bytes(1)?;
                let mut m = BTreeMap::new();
                for _ in 0 .. self.unsigned(OBJECT, n)? {
                    if let Some(k) = Key::from_value(self.cbor(max_recur - 1)?) {
                        m.insert(k, self.cbor(max_recur - 1)?);
                    } else {
                        return Err(DecodeError::InvalidKey)
                    }
                }
                Ok(Cbor::Object(m))
            }
            (major, info) => Err(DecodeError::Type(major, info))
        }
    }

    pub fn skip(&mut self) -> Result<()> {
        let mut nrounds = 1;
        let mut irounds = 0;
        while nrounds > 0 || irounds > 0 {
            match typeinfo(self.next()?) {
                (UNSIGNED, n) => { self.unsigned(UNSIGNED, n)?; }
                (SIGNED,   n) => { self.unsigned(SIGNED, n)?; }
                (TAGGED,   n) => { self.unsigned(TAGGED, n)?; }
                (SIMPLE,  31) => { if irounds > 0 { irounds -= 1 } }
                (SIMPLE,   n) => { self.unsigned(SIMPLE, n)?; }
                (BYTES,   31) => {
                    self.idx -= 1;
                    let mut it = self.bytes_iter()?;
                    for _ in &mut it {}
                    if let Some(e) = it.error.take() {
                        return Err(e)
                    }
                }
                (BYTES,    n) => {
                    let n = self.unsigned(BYTES, n)?;
                    self.read(n as usize)?;
                }
                (TEXT,    31) => {
                    self.idx -= 1;
                    let mut it = self.str_iter()?;
                    for _ in &mut it {}
                    if let Some(e) = it.error.take() {
                        return Err(e)
                    }
                }
                (TEXT,     n) => {
                    let n = self.unsigned(TEXT, n)?;
                    self.read(n as usize)?;
                }
                (ARRAY,   31) => { irounds += 1; }
                (OBJECT,  31) => { irounds += 1; }
                (ARRAY,    n) => {
                    let x = self.unsigned(ARRAY, n)?;
                    if u64::MAX - nrounds < x {
                        return Err(DecodeError::Overflow(x))
                    } else {
                        nrounds += x
                    }
                }
                (OBJECT,   n) => {
                    let x = self.unsigned(OBJECT, n)?;
                    if x > u64::MAX / 2 || u64::MAX - nrounds < 2 * x {
                        return Err(DecodeError::Overflow(x))
                    } else {
                        nrounds += 2 * x
                    }
                }
                (typ,   info) => return Err(DecodeError::Type(typ, info))
            }
            if nrounds > 0 { nrounds -= 1 }
        }
        Ok(())
    }

    #[inline]
    fn read(&mut self, n: usize) -> Result<&'buf [u8]> {
        if self.len - self.idx < n {
            Err(DecodeError::EndOfInput)
        } else {
            let s = &self.buf[self.idx .. self.idx + n];
            self.idx += n;
            Ok(s)
        }
    }

    #[inline]
    fn next(&mut self) -> Result<u8> {
        if self.idx < self.len {
            let x = self.buf[self.idx];
            self.idx += 1;
            Ok(x)
        } else {
            Err(DecodeError::EndOfInput)
        }
    }

    #[inline]
    fn unsigned(&mut self, major: u8, first: u8) -> Result<u64> {
        match first {
            n @ 0...23 => Ok(u64::from(n)),
            24 => self.next().map(u64::from),
            25 => self.read(2).map(BigEndian::read_u16).map(u64::from),
            26 => self.read(4).map(BigEndian::read_u32).map(u64::from),
            27 => self.read(8).map(BigEndian::read_u64),
            _  => Err(DecodeError::Type(major, first))
        }
    }
}

/// Iterator over the chunks of an indefinite bytes item.
pub struct BytesIter<'a, 'buf: 'a> {
    decoder: &'a mut Decoder<'buf>,
    error:   Option<DecodeError>
}

impl<'a, 'buf> BytesIter<'a, 'buf> {
    pub fn new(d: &'a mut Decoder<'buf>) -> BytesIter<'a, 'buf> {
        BytesIter { decoder: d, error: None }
    }
}

impl<'a, 'buf> Iterator for BytesIter<'a, 'buf> {
    type Item = Result<&'buf [u8]>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.decoder.is_break() {
            Err(e) => {
                self.error = Some(e);
                None
            }
            Ok(true) => {
                if let Err(e) = self.decoder.skip_bytes(1) {
                    self.error = Some(e)
                }
                None
            }
            Ok(false) => Some(self.decoder.bytes())
        }
    }
}

/// Iterator over the chunks of an indefinite str item.
pub struct StrIter<'a, 'buf: 'a> {
    decoder: &'a mut Decoder<'buf>,
    error:   Option<DecodeError>
}

impl<'a, 'buf> StrIter<'a, 'buf> {
    pub fn new(d: &'a mut Decoder<'buf>) -> StrIter<'a, 'buf> {
        StrIter { decoder: d, error: None }
    }
}

impl<'a, 'buf> Iterator for StrIter<'a, 'buf> {
    type Item = Result<&'buf str>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.decoder.is_break() {
            Err(e) => {
                self.error = Some(e);
                None
            }
            Ok(true) => {
                if let Err(e) = self.decoder.skip_bytes(1) {
                    self.error = Some(e)
                }
                None
            }
            Ok(false) => Some(self.decoder.str())
        }
    }
}

