extern crate base64;
#[macro_use] extern crate json;
extern crate miel;

use miel::slice::Decoder;
use miel::ast::Cbor;
use json::{Json, FromJson, DecodeResult};
use json::decoder::ReadIter;
use std::{f32, f64};
use std::fs::File;

#[derive(Debug, Clone)]
struct TestVector {
    cbor:       String,
    hex:        String,
    roundtrip:  bool,
    decoded:    Option<Json>,
    diagnostic: Option<Json>
}

impl FromJson for TestVector {
    fn decode<I: Iterator<Item=char>>(d: &mut json::Decoder<I>) -> DecodeResult<TestVector> {
        object! {
            let decoder = d;
            TestVector {
                cbor:       req. "cbor"       => d.string(),
                hex:        req. "hex"        => d.string(),
                roundtrip:  req. "roundtrip"  => d.bool(),
                decoded:    opt. "decoded"    => d.from_json(),
                diagnostic: opt. "diagnostic" => d.from_json()
            }
        }
    }
}

#[test]
fn test_appendix() {
    let iter = ReadIter::new(File::open("tests/appendix_a.json").unwrap());
    let tests: Vec<TestVector> = json::Decoder::default(iter).from_json().unwrap();
    for t in tests {
        let raw = base64::decode(&t.cbor).unwrap();
        let mut dec = Decoder::new(&raw[..]);
        let cbor = dec.cbor(16).unwrap();
        if let Some(json) = t.decoded {
            if !eq(&json, &cbor) {
                panic!("{}: {:?} != {:?}", t.hex, json, cbor)
            }
            continue
        }
        if let Some(Json::String(ref json)) = t.diagnostic {
            if !diag(&json, &cbor) {
                panic!("{}: {:?} != {:?}", t.hex, json, cbor)
            }
        }
    }
}

fn eq(a: &Json, b: &Cbor) -> bool {
    match (a, b) {
        (&Json::Number(x),     y)                    => as_f64(y).map(|i| (x - i).abs() < f64::EPSILON).unwrap_or(false),
        (&Json::Null,          &Cbor::Null)          => true,
        (&Json::Bool(x),       &Cbor::Bool(y))       => x == y,
        (&Json::String(ref x), &Cbor::Str(ref y))    => x == y.as_ref(),
        (&Json::Array(ref x),  &Cbor::Array(ref y))  => x.len() == y.len() && x.iter().zip(y.iter()).all(|(xi, yi)| eq(xi, yi)),
        (&Json::Object(ref x), &Cbor::Object(ref y)) => {
            for (k, v) in x {
                if let Some(w) = y.get(&k.as_str().into()) {
                    if !eq(v, w) { return false }
                } else {
                    return false
                }
            }
            true
        }
        (&Json::String(ref x), &Cbor::StrChunks(ref y)) => {
            let mut s = String::new();
            for c in y {
                s.push_str(c)
            }
            x == &s
        }
        _ => false
    }
}

fn diag(a: &str, b: &Cbor) -> bool {
    match (a, b) {
        ("Infinity",  &Cbor::F32(x))    => x == f32::INFINITY,
        ("Infinity",  &Cbor::F64(x))    => x == f64::INFINITY,
        ("-Infinity", &Cbor::F32(x))    => x == -f32::INFINITY,
        ("-Infinity", &Cbor::F64(x))    => x == -f64::INFINITY,
        ("NaN",       &Cbor::F32(x))    => x.is_nan(),
        ("NaN",       &Cbor::F64(x))    => x.is_nan(),
        ("undefined", &Cbor::Undefined) => true,
        _                               => true
    }
}

pub fn as_f64(x: &Cbor) -> Option<f64> {
    match *x {
        Cbor::U8(n)  => Some(n as f64),
        Cbor::U16(n) => Some(n as f64),
        Cbor::U32(n) => Some(n as f64),
        Cbor::U64(n) => Some(n as f64),
        Cbor::I8(n)  => Some(n as f64),
        Cbor::I16(n) => Some(n as f64),
        Cbor::I32(n) => Some(n as f64),
        Cbor::I64(n) => Some(n as f64),
        Cbor::F16(n) => Some(n as f64),
        Cbor::F32(n) => Some(n as f64),
        Cbor::F64(n) => Some(n),
        _            => None
    }
}

