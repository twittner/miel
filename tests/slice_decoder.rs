extern crate hex;
extern crate miel;

use miel::{Tag, Ref};
use miel::slice::Decoder;
use std::collections::BTreeMap;
use std::{f32, f64};

#[test]
fn unsigned() {
    assert_eq!(Some(0), decode(b"00", |d| d.u8().ok()));
    assert_eq!(Some(1), decode(b"01", |d| d.u8().ok()));
    assert_eq!(Some(10), decode(b"0a", |d| d.u8().ok()));
    assert_eq!(Some(23), decode(b"17", |d| d.u8().ok()));
    assert_eq!(Some(24), decode(b"1818", |d| d.u8().ok()));
    assert_eq!(Some(25), decode(b"1819", |d| d.u8().ok()));
    assert_eq!(Some(100), decode(b"1864", |d| d.u8().ok()));
    assert_eq!(Some(1000), decode(b"1903e8", |d| d.u16().ok()));
    assert_eq!(Some(1000000), decode(b"1a000f4240", |d| d.u32().ok()));
    assert_eq!(Some(1000000000000), decode(b"1b000000e8d4a51000", |d| d.u64().ok()));
    assert_eq!(Some(18446744073709551615), decode(b"1bffffffffffffffff", |d| d.u64().ok()));
}

#[test]
fn signed() {
    assert_eq!(Some(-1), decode(b"20", |d| d.i8().ok()));
    assert_eq!(Some(-10), decode(b"29", |d| d.i8().ok()));
    assert_eq!(Some(-100), decode(b"3863", |d| d.i8().ok()));
    assert_eq!(Some(-500), decode(b"3901f3", |d| d.i16().ok()));
    assert_eq!(Some(-1000), decode(b"3903e7", |d| d.i16().ok()));
    assert_eq!(Some(-343434), decode(b"3a00053d89", |d| d.i32().ok()));
    assert_eq!(Some(-23764523654), decode(b"3b000000058879da85", |d| d.i64().ok()))
}

#[test]
fn mixed() {
    assert_eq!(Some(0), decode(b"00", |d| d.i8().ok()));
    assert_eq!(Some(1), decode(b"01", |d| d.i8().ok()));
    assert_eq!(Some(10), decode(b"0a", |d| d.i8().ok()));
    assert_eq!(Some(23), decode(b"17", |d| d.i8().ok()));
    assert_eq!(Some(24), decode(b"1818", |d| d.i8().ok()));
    assert_eq!(Some(25), decode(b"1819", |d| d.i8().ok()));
    assert_eq!(Some(100), decode(b"1864", |d| d.i8().ok()));
    assert_eq!(Some(1000), decode(b"1903e8", |d| d.i16().ok()));
    assert_eq!(Some(1000000), decode(b"1a000f4240", |d| d.i32().ok()));
}

#[test]
fn float() {
    assert_eq!(Some(0.0), decode(b"f90000", |d| d.f16().ok()));
    assert_eq!(Some(-0.0), decode(b"f98000", |d| d.f16().ok()));
    assert_eq!(Some(1.0), decode(b"f93c00", |d| d.f16().ok()));
    assert_eq!(Some(1.5), decode(b"f93e00", |d| d.f16().ok()));
    assert_eq!(Some(65504.0), decode(b"f97bff", |d| d.f16().ok()));
    assert_eq!(Some(f32::INFINITY), decode(b"f97c00", |d| d.f16().ok()));
    assert_eq!(Some(-f32::INFINITY), decode(b"f9fc00", |d| d.f16().ok()));
    assert!(decode(b"f97e00", |d| d.f16().ok().unwrap().is_nan()));

    assert_eq!(Some(100000.0), decode(b"fa47c35000", |d| d.f32().ok()));
    assert_eq!(Some(3.4028234663852886e+38), decode(b"fa7f7fffff", |d| d.f32().ok()));
    assert_eq!(Some(-4.1), decode(b"fbc010666666666666", |d| d.f64().ok()));

    assert_eq!(Some(f32::INFINITY), decode(b"fa7f800000", |d| d.f32().ok()));
    assert_eq!(Some(-f32::INFINITY), decode(b"faff800000", |d| d.f32().ok()));
    assert!(decode(b"fa7fc00000", |d| d.f32().ok().unwrap().is_nan()));

    assert_eq!(Some(1.0e+300), decode(b"fb7e37e43c8800759c", |d| d.f64().ok()));
    assert_eq!(Some(f64::INFINITY), decode(b"fb7ff0000000000000", |d| d.f64().ok()));
    assert_eq!(Some(-f64::INFINITY), decode(b"fbfff0000000000000", |d| d.f64().ok()));
    assert!(decode(b"fb7ff8000000000000", |d| d.f64().ok().unwrap().is_nan()))
}

#[test]
fn bool() {
    assert_eq!(Some(false), decode(b"f4", |d| d.bool().ok()));
    assert_eq!(Some(true), decode(b"f5", |d| d.bool().ok()));
}

#[test]
fn simple() {
    assert_eq!(Some(16), decode(b"f0", |d| d.simple().ok()));
    assert_eq!(Some(255), decode(b"f8ff", |d| d.simple().ok()))
}

#[test]
fn bytes() {
    decode(b"4401020304", |d| {
        assert_eq!(Some(&[1,2,3,4][..]), d.bytes().ok())
    })
}

#[test]
fn text() {
    decode(b"781f64667364667364660d0a7364660d0a68656c6c6f0d0a736466736673646673", |d| {
        assert_eq!(Some("dfsdfsdf\r\nsdf\r\nhello\r\nsdfsfsdfs"), d.str().ok());
    });
    decode(b"62c3bc", |d| assert_eq!(Some("\u{00fc}"), d.str().ok()));
}

#[test]
fn option() {
    assert!(decode(b"f6", |d| d.is_null().unwrap()));
    assert!(decode(b"01", |d| !d.is_null().unwrap()));
}

#[test]
fn undefined() {
    assert!(decode(b"f7", |d| d.is_undefined().unwrap()));
    assert!(decode(b"01", |d| !d.is_undefined().unwrap()));
}

#[test]
fn empty_array() {
    assert_eq!(None, decode(b"9fff", |d| d.array().unwrap()))
}

#[test]
fn array() {
    decode(b"83010203", |d| {
        assert_eq!(Some(3), d.array().unwrap());
        assert_eq!(Some(1u32), d.u32().ok());
        assert_eq!(Some(2u32), d.u32().ok());
        assert_eq!(Some(3u32), d.u32().ok());
    })
}

#[test]
fn object() {
    let mut map = BTreeMap::new();
    map.insert(String::from("a"), 1u8);
    map.insert(String::from("b"), 2u8);
    map.insert(String::from("c"), 3u8);
    let mut x = BTreeMap::new();
    decode(b"a3616101616202616303", |d| {
        for _ in 0 .. d.object().unwrap().unwrap() {
            x.insert(d.str().unwrap().to_string(), d.u8().unwrap());
        }
    });
    assert_eq!(map, x);
}

#[test]
fn skip() {
    decode(b"a66161016162820203616382040561647f657374726561646d696e67ff61659f070405ff61666568656c6c6f", |d| {
        for _ in 0 .. d.object().unwrap().unwrap() {
            match d.str().unwrap() {
                "a" => { d.u8().unwrap(); }
                "b" => for _ in 0 .. d.array().unwrap().unwrap() { d.u8().unwrap(); },
                "c" => d.skip().unwrap(),
                "d" => d.skip().unwrap(),
                "e" => d.skip().unwrap(),
                "f" => d.skip().unwrap(),
                key => panic!("unexpected key: {}", key)
            }
        }
    })
}

#[test]
fn array_of_array() {
    decode(b"828301020383010203", |d| {
        let outer = d.array().unwrap().unwrap();
        let mut v = Vec::with_capacity(outer);
        for _ in 0 .. outer {
            let inner = d.array().unwrap().unwrap();
            let mut w = Vec::with_capacity(inner);
            for _ in 0 .. inner {
                w.push(d.u8().unwrap())
            }
            v.push(w)
        }
        assert_eq!(vec![vec![1,2,3], vec![1,2,3]], v)
    });
}

#[test]
fn tagged() {
    decode(b"c11a514b67b0", |d| {
        assert_eq!(Some(Tag::Timestamp), d.tag().ok());
        assert_eq!(Some(1363896240), d.u32().ok())
    });
}

#[test]
fn cbor() {
    decode(b"a2616101028103", |d| {
        let v = d.cbor(8).unwrap();
        let r = Ref::new(&v);
        assert_eq!(Some(1), r.get("a").unsigned());
        assert_eq!(Some(3), r.get(2).at(0).unsigned())
    });
}

fn decode<T, F: FnMut(&mut Decoder) -> T>(b: &[u8], mut f: F) -> T {
    let v = hex::decode(b).unwrap();
    let mut d = Decoder::new(&v);
    f(&mut d)
}

