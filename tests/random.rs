extern crate quickcheck;
extern crate miel;
extern crate rand;

use miel::{ast::{Cbor, Key}, slice::Decoder, write::Encoder, types::Tag};
use quickcheck::{Arbitrary, Gen, QuickCheck, StdGen};
use rand::Rng;
use std::{borrow::Cow, collections::BTreeMap};

const DEPTH: usize = 32;

#[derive(Clone, Debug)]
pub struct ArbitraryCbor(Cbor<'static>);

impl Arbitrary for ArbitraryCbor {
    fn arbitrary<G: Gen>(g: &mut G) -> ArbitraryCbor {
        ArbitraryCbor(gen_cbor(DEPTH, g))
    }
}

#[test]
fn cbor_identity() {
    fn property(a: ArbitraryCbor) -> bool {
        let mut buf = Vec::new();
        Encoder::new(&mut buf).cbor(&a.0).unwrap();
        let b = Decoder::new(&buf).cbor(DEPTH).unwrap();
        eq(&a.0, &b)
    }
    QuickCheck::new()
        .tests(10000)
        .gen(StdGen::new(rand::thread_rng(), 255))
        .quickcheck(property as fn(ArbitraryCbor) -> bool)
}

fn eq(a: &Cbor, b: &Cbor) -> bool {
    match (a, b) {
        (&Cbor::Array(ref x), &Cbor::Array(ref y)) => x.len() == y.len() && x.iter().zip(y.iter()).all(|(vx, vy)| eq(vx, vy)),
        (&Cbor::Bool(x), &Cbor::Bool(y))           => x == y,
        (&Cbor::Bytes(ref x), &Cbor::Bytes(ref y)) => x == y,
        (&Cbor::BytesChunks(ref x), &Cbor::BytesChunks(ref y)) => x == y,
        (&Cbor::Str(ref x), &Cbor::Str(ref y))   => x == y,
        (&Cbor::StrChunks(ref x), &Cbor::StrChunks(ref y))   => x == y,
        (&Cbor::Simple(x), &Cbor::Simple(y))       => x == y,
        (&Cbor::Null, &Cbor::Null)                 => true,
        (&Cbor::Undefined, &Cbor::Undefined)       => true,
        (&Cbor::F16(x), &Cbor::F16(y))             => x.is_nan() && y.is_nan() || x == y,
        (&Cbor::F32(x), &Cbor::F32(y))             => x.is_nan() && y.is_nan() || x == y,
        (&Cbor::F64(x), &Cbor::F64(y))             => x.is_nan() && y.is_nan() || x == y,
        (&Cbor::Object(ref x), &Cbor::Object(ref y)) => x.len() == y.len() && x.iter().zip(y.iter()).all(|((kx, vx), (ky, vy))| eq_key(kx, ky) && eq(vx, vy)),
        (&Cbor::Tagged(ta, ref x), &Cbor::Tagged(tb, ref y)) => ta == tb && eq(&*x, &*y),
        (&Cbor::U8(x),  y) => eq_unsigned(x as u64, y),
        (&Cbor::U16(x), y) => eq_unsigned(x as u64, y),
        (&Cbor::U32(x), y) => eq_unsigned(x as u64, y),
        (&Cbor::U64(x), y) => eq_unsigned(x, y),
        (&Cbor::I8(x),  y) if x < 0 => eq_signed(x as i64, y),
        (&Cbor::I16(x), y) if x < 0 => eq_signed(x as i64, y),
        (&Cbor::I32(x), y) if x < 0 => eq_signed(x as i64, y),
        (&Cbor::I64(x), y) if x < 0 => eq_signed(x, y),
        (&Cbor::I8(x),  y) => eq_unsigned(x as u64, y),
        (&Cbor::I16(x), y) => eq_unsigned(x as u64, y),
        (&Cbor::I32(x), y) => eq_unsigned(x as u64, y),
        (&Cbor::I64(x), y) => eq_unsigned(x as u64, y),
        _ => false
    }
}

fn eq_key(a: &Key, b: &Key) -> bool {
    match (a, b) {
        (&Key::Int(x), &Key::Int(y)) => x == y,
        (&Key::Str(ref x), &Key::Str(ref y)) => x == y,
        (&Key::Bytes(ref x), &Key::Bytes(ref y)) => x == y,
        _ => false
    }
}

fn eq_unsigned(a: u64, b: &Cbor) -> bool {
    match *b {
        Cbor::U8(x)  => a == x as u64,
        Cbor::U16(x) => a == x as u64,
        Cbor::U32(x) => a == x as u64,
        Cbor::U64(x) => a == x,
        _            => false
    }
}

fn eq_signed(a: i64, b: &Cbor) -> bool {
    match *b {
        Cbor::I8(x)  => a == x as i64,
        Cbor::I16(x) => a == x as i64,
        Cbor::I32(x) => a == x as i64,
        Cbor::I64(x) => a == x,
        _            => false
    }
}

fn gen_cbor<'a, G: Gen>(level: usize, g: &mut G) -> Cbor<'a> {
    match g.gen_range(0, 20) {
        0  => Cbor::Null,
        1  => Cbor::Undefined,
        2  => Cbor::U8(g.gen()),
        3  => Cbor::U16(g.gen()),
        4  => Cbor::U32(g.gen()),
        5  => Cbor::U64(g.gen()),
        6  => Cbor::I8(g.gen()),
        7  => Cbor::I16(g.gen()),
        8  => Cbor::I32(g.gen()),
        9  => Cbor::I64(g.gen()),
        10 => Cbor::F32(g.gen()),
        11 => Cbor::F64(g.gen()),
        12 => Cbor::Str(gen_str(g)),
        13 => Cbor::StrChunks(gen_str_chunks(g)),
        14 => Cbor::Bytes(gen_bytes(g)),
        15 => Cbor::BytesChunks(gen_bytes_chunks(g)),
        16 => Cbor::Array(if level > 0 { gen_array(level - 1, g) } else { Vec::new() }),
        17 => Cbor::Bool(g.gen()),
        18 => Cbor::Simple(gen_simple(g)),
        19 => Cbor::Object(if level > 0 { gen_object(level - 1, g) } else { BTreeMap::new() }),
        _  => gen_tagged(g)
    }
}

fn gen_str<'a, G: Gen>(g: &mut G) -> Cow<'a, str> {
    Cow::Owned(Arbitrary::arbitrary(g))
}

fn gen_bytes<'a, G: Gen>(g: &mut G) -> Cow<'a, [u8]> {
    Cow::Owned(Arbitrary::arbitrary(g))
}

fn gen_array<'a, G: Gen>(level: usize, g: &mut G) -> Vec<Cbor<'a>> {
    let len = g.gen_range(0, 16);
    let mut vec = Vec::with_capacity(len);
    for _ in 0 .. len {
        vec.push(gen_cbor(level, g))
    }
    vec
}

fn gen_object<'a, G: Gen>(level: usize, g: &mut G) -> BTreeMap<Key<'a>, Cbor<'a>> {
    let     len = g.gen_range(0, 16);
    let mut map = BTreeMap::new();
    for _ in 0 .. len {
        map.insert(gen_key(g), gen_cbor(level, g));
    }
    map
}

fn gen_key<'a, G: Gen>(g: &mut G) -> Key<'a> {
    match g.gen_range(0, 3) {
        1 => Key::Str(gen_str(g)),
        2 => Key::Bytes(gen_bytes(g)),
        _ => Key::Int(g.gen())
    }
}

fn gen_str_chunks<'a, G: Gen>(g: &mut G) -> Vec<Cow<'a, str>> {
    let len = g.gen_range(0, 12);
    let mut vec = Vec::with_capacity(len);
    for _ in 0 .. len {
        vec.push(gen_str(g))
    }
    vec
}

fn gen_bytes_chunks<'a, G: Gen>(g: &mut G) -> Vec<Cow<'a, [u8]>> {
    let len = g.gen_range(0, 12);
    let mut vec = Vec::with_capacity(len);
    for _ in 0 .. len {
        vec.push(gen_bytes(g))
    }
    vec
}

fn gen_simple<G: Gen>(g: &mut G) -> u8 {
    if g.gen() {
        g.gen_range(0, 19)
    } else {
        g.gen_range(32, 255)
    }
}

fn gen_tagged<'a, G: Gen>(g: &mut G) -> Cbor<'a> {
    match gen_tag(g) {
        t@Tag::DateTime  => Cbor::Tagged(t, Box::new(Cbor::Str(gen_str(g)))),
        t@Tag::Timestamp => match g.gen_range(0, 10) {
            0 => Cbor::Tagged(t, Box::new(Cbor::U8(g.gen()))),
            1 => Cbor::Tagged(t, Box::new(Cbor::U16(g.gen()))),
            2 => Cbor::Tagged(t, Box::new(Cbor::U32(g.gen()))),
            3 => Cbor::Tagged(t, Box::new(Cbor::U64(g.gen()))),
            4 => Cbor::Tagged(t, Box::new(Cbor::I8(g.gen()))),
            5 => Cbor::Tagged(t, Box::new(Cbor::I16(g.gen()))),
            6 => Cbor::Tagged(t, Box::new(Cbor::I32(g.gen()))),
            7 => Cbor::Tagged(t, Box::new(Cbor::I64(g.gen()))),
            8 => Cbor::Tagged(t, Box::new(Cbor::F32(g.gen()))),
            _ => Cbor::Tagged(t, Box::new(Cbor::F64(g.gen())))
        },
        t@Tag::Bignum         => Cbor::Tagged(t, Box::new(Cbor::Bytes(gen_bytes(g)))),
        t@Tag::NegativeBignum => Cbor::Tagged(t, Box::new(Cbor::Bytes(gen_bytes(g)))),
        t@Tag::Uri            => Cbor::Tagged(t, Box::new(Cbor::Str(gen_str(g)))),
        t@Tag::Base64         => Cbor::Tagged(t, Box::new(Cbor::Str(gen_str(g)))),
        t@Tag::ToBase64Url    => Cbor::Tagged(t, Box::new(Cbor::Str(gen_str(g)))),
        t@Tag::Regex          => Cbor::Tagged(t, Box::new(Cbor::Str(gen_str(g)))),
        t@Tag::Decimal        => Cbor::Tagged(t, Box::new(Cbor::Array(vec![Cbor::U64(g.gen()), Cbor::U64(g.gen())]))),
        t@Tag::Bigfloat       => Cbor::Tagged(t, Box::new(Cbor::Array(vec![Cbor::U64(g.gen()), Cbor::U64(g.gen())]))),
        _                     => Cbor::Tagged(Tag::Mime, Box::new(Cbor::Str(gen_str(g))))
    }
}

fn gen_tag<G: Gen>(g: &mut G) -> Tag {
    match g.gen_range(0, 20) {
        0  => Tag::DateTime,
        1  => Tag::Timestamp,
        2  => Tag::Bignum,
        3  => Tag::NegativeBignum,
        4  => Tag::Decimal,
        5  => Tag::Bigfloat,
        6  => Tag::ToBase64Url,
        7  => Tag::ToBase64,
        8  => Tag::ToBase16,
        9  => Tag::Cbor,
        10 => Tag::Uri,
        11 => Tag::Base64Url,
        12 => Tag::Base64,
        13 => Tag::Regex,
        14 => Tag::Mime,
        15 => Tag::CborSelf,
        tg => Tag::Unassigned(tg)
    }
}

