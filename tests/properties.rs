extern crate miel;
extern crate quickcheck;

use miel::{slice::Decoder, write::Encoder};
use quickcheck::QuickCheck;
use std::{i8, i16, i32, i64, f32, f64};

#[test]
fn identity_u8() {
    fn prop(x: u8) -> bool {
        let mut w = Vec::new();

        Encoder::new(&mut w).u8(x).unwrap();
        assert_eq!(x, Decoder::new(&w).u8().unwrap());

        Encoder::new(&mut w).u16(u16::from(x)).unwrap();
        assert_eq!(x, Decoder::new(&w).u8().unwrap());

        Encoder::new(&mut w).u32(u32::from(x)).unwrap();
        assert_eq!(x, Decoder::new(&w).u8().unwrap());

        Encoder::new(&mut w).u64(u64::from(x)).unwrap();
        x == Decoder::new(&w).u8().unwrap()
    }
    QuickCheck::new().quickcheck(prop as fn(u8) -> bool)
}

#[test]
fn identity_u16() {
    fn prop(x: u16) -> bool {
        let mut w = Vec::new();

        Encoder::new(&mut w).u16(x).unwrap();
        assert_eq!(x, Decoder::new(&w).u16().unwrap());

        Encoder::new(&mut w).u32(u32::from(x)).unwrap();
        assert_eq!(x, Decoder::new(&w).u16().unwrap());

        Encoder::new(&mut w).u64(u64::from(x)).unwrap();
        x == Decoder::new(&w).u16().unwrap()
    }
    QuickCheck::new().quickcheck(prop as fn(u16) -> bool)
}

#[test]
fn identity_u32() {
    fn prop(x: u32) -> bool {
        let mut w = Vec::new();

        Encoder::new(&mut w).u32(x).unwrap();
        assert_eq!(x, Decoder::new(&w).u32().unwrap());

        Encoder::new(&mut w).u64(u64::from(x)).unwrap();
        x == Decoder::new(&w).u32().unwrap()
    }
    QuickCheck::new().quickcheck(prop as fn(u32) -> bool)
}

#[test]
fn identity_u64() {
    fn prop(x: u64) -> bool {
        let mut w = Vec::new();
        Encoder::new(&mut w).u64(x).unwrap();
        x == Decoder::new(&w).u64().unwrap()
    }
    QuickCheck::new().quickcheck(prop as fn(u64) -> bool)
}

#[test]
fn identity_i8() {
    fn prop(x: i8) -> bool {
        let mut w = Vec::new();

        Encoder::new(&mut w).i8(x).unwrap();
        assert_eq!(x, Decoder::new(&w).i8().unwrap());

        Encoder::new(&mut w).i16(i16::from(x)).unwrap();
        assert_eq!(x, Decoder::new(&w).i8().unwrap());

        Encoder::new(&mut w).i32(i32::from(x)).unwrap();
        assert_eq!(x, Decoder::new(&w).i8().unwrap());

        Encoder::new(&mut w).i64(i64::from(x)).unwrap();
        x == Decoder::new(&w).i8().unwrap()
    }
    QuickCheck::new().quickcheck(prop as fn(i8) -> bool)
}

#[test]
fn identity_i16() {
    fn prop(x: i16) -> bool {
        let mut w = Vec::new();

        Encoder::new(&mut w).i16(x).unwrap();
        assert_eq!(x, Decoder::new(&w).i16().unwrap());

        Encoder::new(&mut w).i32(i32::from(x)).unwrap();
        assert_eq!(x, Decoder::new(&w).i16().unwrap());

        Encoder::new(&mut w).i64(i64::from(x)).unwrap();
        x == Decoder::new(&w).i16().unwrap()
    }
    QuickCheck::new().quickcheck(prop as fn(i16) -> bool)
}

#[test]
fn identity_i32() {
    fn prop(x: i32) -> bool {
        let mut w = Vec::new();

        Encoder::new(&mut w).i32(x).unwrap();
        assert_eq!(x, Decoder::new(&w).i32().unwrap());

        Encoder::new(&mut w).i64(i64::from(x)).unwrap();
        x == Decoder::new(&w).i32().unwrap()
    }
    QuickCheck::new().quickcheck(prop as fn(i32) -> bool)
}

#[test]
fn identity_i64() {
    fn prop(x: i64) -> bool {
        let mut w = Vec::new();
        Encoder::new(&mut w).i64(x).unwrap();
        x == Decoder::new(&w).i64().unwrap()
    }
    QuickCheck::new().quickcheck(prop as fn(i64) -> bool)
}

#[test]
fn identity_u8_i8() {
    fn prop(x: u8) -> bool {
        let y = x % i8::MAX as u8;
        let mut w = Vec::new();
        Encoder::new(&mut w).u8(y).unwrap();
        y == Decoder::new(&w).i8().unwrap() as u8
    }
    QuickCheck::new().quickcheck(prop as fn(u8) -> bool)
}

#[test]
fn identity_u16_i16() {
    fn prop(x: u16) -> bool {
        let y = x % i16::MAX as u16;
        let mut w = Vec::new();
        Encoder::new(&mut w).u16(y).unwrap();
        y == Decoder::new(&w).i16().unwrap() as u16
    }
    QuickCheck::new().quickcheck(prop as fn(u16) -> bool)
}

#[test]
fn identity_u32_i32() {
    fn prop(x: u32) -> bool {
        let y = x % i32::MAX as u32;
        let mut w = Vec::new();
        Encoder::new(&mut w).u32(y).unwrap();
        y == Decoder::new(&w).i32().unwrap() as u32
    }
    QuickCheck::new().quickcheck(prop as fn(u32) -> bool)
}

#[test]
fn identity_u64_i64() {
    fn prop(x: u64) -> bool {
        let y = x % i64::MAX as u64;
        let mut w = Vec::new();
        Encoder::new(&mut w).u64(y).unwrap();
        y == Decoder::new(&w).i64().unwrap() as u64
    }
    QuickCheck::new().quickcheck(prop as fn(u64) -> bool)
}

#[test]
fn identity_f32() {
    fn prop(x: f32) -> bool {
        let mut w = Vec::new();
        Encoder::new(&mut w).f32(x).unwrap();
        x == Decoder::new(&w).f32().unwrap()
    }
    QuickCheck::new().quickcheck(prop as fn(f32) -> bool)
}

#[test]
fn identity_f64() {
    fn prop(x: f64) -> bool {
        let mut w = Vec::new();
        Encoder::new(&mut w).f64(x).unwrap();
        x == Decoder::new(&w).f64().unwrap()
    }
    QuickCheck::new().quickcheck(prop as fn(f64) -> bool)
}

#[test]
fn identity_bytes() {
    fn prop(x: Vec<u8>) -> bool {
        let mut w = Vec::new();
        Encoder::new(&mut w).bytes(&x).unwrap();
        x == Decoder::new(&w).bytes().unwrap()
    }
    QuickCheck::new().quickcheck(prop as fn(Vec<u8>) -> bool)
}

#[test]
fn int_min_max() {
    let mut w = Vec::new();
    Encoder::new(&mut w).i8(i8::MAX).unwrap();
    assert_eq!(i8::MAX, Decoder::new(&w).i8().unwrap());

    w.clear();
    Encoder::new(&mut w).i8(i8::MIN).unwrap();
    assert_eq!(i8::MIN, Decoder::new(&w).i8().unwrap());

    w.clear();
    Encoder::new(&mut w).i16(i16::MAX).unwrap();
    assert_eq!(i16::MAX, Decoder::new(&w).i16().unwrap());

    w.clear();
    Encoder::new(&mut w).i16(i16::MIN).unwrap();
    assert_eq!(i16::MIN, Decoder::new(&w).i16().unwrap());

    w.clear();
    Encoder::new(&mut w).i32(i32::MAX).unwrap();
    assert_eq!(i32::MAX, Decoder::new(&w).i32().unwrap());

    w.clear();
    Encoder::new(&mut w).i32(i32::MIN).unwrap();
    assert_eq!(i32::MIN, Decoder::new(&w).i32().unwrap());

    w.clear();
    Encoder::new(&mut w).i64(i64::MAX).unwrap();
    assert_eq!(i64::MAX, Decoder::new(&w).i64().unwrap());

    w.clear();
    Encoder::new(&mut w).i64(i64::MIN).unwrap();
    assert_eq!(i64::MIN, Decoder::new(&w).i64().unwrap());
}

