extern crate hex;
extern crate miel;

use miel::{EncodeError, Tag};
use miel::write::Encoder;
use std::{f32, f64};

#[test]
fn unsigned() {
    encoded(b"00", |e| e.u8(0));
    encoded(b"01", |e| e.u8(1));
    encoded(b"0a", |e| e.u8(10));
    encoded(b"17", |e| e.u8(23));
    encoded(b"1818", |e| e.u8(24));
    encoded(b"1819", |e| e.u8(25));
    encoded(b"1864", |e| e.u8(100));
    encoded(b"1903e8", |e| e.u16(1000));
    encoded(b"1a000f4240", |e| e.u32(1000000));
    encoded(b"1b000000e8d4a51000", |e| e.u64(1000000000000));
    encoded(b"1bffffffffffffffff", |e| e.u64(18446744073709551615))
}

#[test]
fn signed() {
    encoded(b"20", |e| e.i8(-1));
    encoded(b"29", |e| e.i8(-10));
    encoded(b"3863", |e| e.i8(-100));
    encoded(b"3901f3", |e| e.i16(-500));
    encoded(b"3903e7", |e| e.i16(-1000));
    encoded(b"3a00053d89", |e| e.i32(-343434));
    encoded(b"3b000000058879da85", |e| e.i64(-23764523654))
}

#[test]
fn bool() {
    encoded(b"f4", |e| e.bool(false));
    encoded(b"f5", |e| e.bool(true))
}

#[test]
fn simple() {
    encoded(b"f0", |e| e.simple(16));
    encoded(b"f8ff", |e| e.simple(255))
}

#[test]
fn float() {
    encoded(b"fa47c35000", |e| e.f32(100000.0));
    encoded(b"fa7f7fffff", |e| e.f32(3.4028234663852886e+38));
    encoded(b"fbc010666666666666", |e| e.f64(-4.1));

    encoded(b"fa7f800000", |e| e.f32(f32::INFINITY));
    encoded(b"faff800000", |e| e.f32(-f32::INFINITY));
    encoded(b"fa7fc00000", |e| e.f32(f32::NAN));

    encoded(b"fb7ff0000000000000", |e| e.f64(f64::INFINITY));
    encoded(b"fbfff0000000000000", |e| e.f64(-f64::INFINITY));
    encoded(b"fb7ff8000000000000", |e| e.f64(f64::NAN));
}

#[test]
fn bytes() {
    encoded(b"4401020304", |e| e.bytes(&vec![1,2,3,4][..]));
}

#[test]
fn str() {
    encoded(b"62c3bc", |e| e.str("\u{00fc}"));
    encoded(b"781f64667364667364660d0a7364660d0a68656c6c6f0d0a736466736673646673", |e| {
        e.str("dfsdfsdf\r\nsdf\r\nhello\r\nsdfsfsdfs")
    });
}

#[test]
fn indefinite_str() {
    encoded(b"7f657374726561646d696e67ff", |e| {
        e.str_begin()?.str("strea")?.str("ming")?.end()
    })
}

#[test]
fn indefinite_bytes() {
    encoded(b"5f457374726561446d696e67ff", |e| {
        e.bytes_begin()?.bytes(b"strea")?.bytes(b"ming")?.end()
    })
}

#[test]
fn option() {
    encoded(b"f6", |e| e.null())
}

#[test]
fn tagged() {
    encoded(b"c11a514b67b0", |e| {
        e.tag(Tag::Timestamp)?.u32(1363896240)
    })
}

#[test]
fn array() {
    encoded(b"83010203", |e| {
        e.array(3)?
         .u32(1)?
         .u32(2)?
         .u32(3)
    });
    encoded(b"8301820203820405", |e| {
        e.array(3)?
         .u8(1)?
         .array(2)?.u8(2)?.u8(3)?
         .array(2)?.u8(4)?.u8(5)
    })
}

#[test]
fn object() {
    encoded(b"a26161016162820203", |e| {
        e.object(2)?
         .str("a")?.u8(1)?
         .str("b")?.array(2)?.u8(2)?.u8(3)
    })
}

fn encoded<F>(expected: &[u8], mut f: F)
    where F: for<'a, 'b> FnMut(&'a mut Encoder<&'b mut Vec<u8>>) -> Result<&'a mut Encoder<&'b mut Vec<u8>>, EncodeError>
{
    let mut buffer = Vec::new();
    {
        let mut encoder = Encoder::new(&mut buffer);
        assert!(f(&mut encoder).is_ok());
    }
    assert_eq!(hex::decode(expected).unwrap(), &buffer[0 .. expected.len() / 2])
}

